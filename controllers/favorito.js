//acciones de nuestro controlador
'use strinct'

var Favorito = require('../models/favorito');

function prueba(req, res){
	if(req.params.var)
	{
		var nombre = req.params.var;
	}
	else
		{
			var nombre="SIN NOMBRE";
		}

	res.status(200).send({
		data:[nombre,3,4],
		message: nombre
		});
}	

//metodos

function getFavoritos(req, res)// recibe request devuelve respuesta
{
	//var favoritoId = req.params.id;
	//res.status(200).send({data: favoritoId});

	Favorito.find({}, (err, favoritos) => {
		if(err){
			res.status(500).send({message: 'error al listar marcador'});//error en el servidor
		}

		if(!favoritos){
			res.status(404).send({message: 'No hay marcadores'});
		}

		res.status(200).send({favoritos});//datos guardados
	})
}

function getFavorito(req, res)
{
	var favoritoId= req.params.id;

	Favorito.findById(favoritoId, function(err, favorito){
		if(err){
			res.status(500).send({message: 'error al mostrar el arcador'});//error en el servidor
		}
		if(!favorito)
		{
			res.status(404).send({message: 'No hay marcador'});
		}

		res.status(200).send({favorito});
	});
}

function saveFavorito(req, res)
{
	var favorito = new Favorito();//nuevo objeto

	var params = req.body;
	
	favorito.title = params.title;
	favorito.description  = params.description;
	favorito.url = params.url;

	//favorito.save();

	favorito.save((err,favoritoStored) => {//funcion callback
		if(err){
			res.status(500).send({message: 'error al guardar el marcador'});//error en el servidor
		}
		res.status(200).send({favorito: favoritoStored});//datos guardados
	});


//res.status(200).send({update: true, favorito: params});
}


function updateFavorito(req, res)
{
	var favoritoId= req.params.id;//parametros por url
	var update = req.body;//variabla

	console.log(update);

	Favorito.findByIdAndUpdate(favoritoId, update, (err, favoritoUpdated) => {
		if(err){
			res.status(500).send({message: 'error al actualizar el marcador'});//error en el servidor
		}
		res.status(200).send({favorito: favoritoUpdated});//datos guardados
	});//metodo para buscar y modificar

}


function deleteFavorito(req, res)
{
	var favoritoId = req.params.id;
	Favorito.findById(favoritoId, function(err, favorito){
		if(err){
			res.status(500).send({message: 'error al mostrar el arcador'});//error en el servidor
		}
		if(!favorito)
		{
			res.status(404).send({message: 'No hay marcador'});
		}
		else
		{
			favorito.remove(err => {
				if(err)
				{
					res.status(200).send({message: ' Error al eliminar!!'});
				}
				else
					{
						res.status(200).send({message: ' el marcador se ha eliminado!!'});
					}
			});

		}

	});
};



module.exports = {// objeto de funcion
	prueba,
	getFavorito,
	getFavoritos, //ruta a metodo
	saveFavorito,
	updateFavorito,
	deleteFavorito
}