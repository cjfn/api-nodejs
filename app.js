'use strict'

var express = require('express');//SERVIDOR DE DEPENDENCIAS
var bodyParser = require('body-parser');

var app = express();// variable referente a express
var api = require('./routes/favorito');

app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());
app.use('/api', api);

//app.get('/prueba/:var?', function(req, res)


module.exports = app;