'use strict'

var mongoose = require('mongoose');// objeto mongoose 
var app = require('./app');
var port = process.env.PORT || 3687;// variable para puerto de servidro

mongoose.connect('mongodb://localhost:27017/cursofavoritos', (err, res) => {
	if(err)
	{
		throw err;// error coneccion
	}
	else 
	{
		console.log('Conexion a Mongo DB CORRECTA');
		app.listen(port,function() {
		console.log("api rest works"+port);
		});	
	}
	
});

