'use strict'// funcionalidad de node xpress

var express = require('express');
var FavoritoController = require('../controllers/favorito');
var api = express.Router();

//definicion de rutas
api.get('/prueba/:var?', FavoritoController.prueba);//ejemplo
api.get('/favorito/:id?', FavoritoController.getFavorito);
api.get('/favoritos', FavoritoController.getFavoritos);
api.post('/favorito', FavoritoController.saveFavorito);
api.put('/favorito/:id', FavoritoController.updateFavorito);
api.delete('/favorito/:id', FavoritoController.deleteFavorito);
module.exports = api; 